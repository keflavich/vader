# Parameter file for Pringle (1981) viscous ring test

# General parameters
nr	    	= 4096
rmin		= 1.5e12
rmax		= 1.5e14
grid_type	= linear
alpha		= c_func
rot_curve_type	= keplerian
rot_curve_mass	= 1.99e33
ibc_pres_type	= fixed_torque
ibc_pres_val	= c_func
ibc_enth_type	= fixed_value
obc_pres_type	= fixed_torque
obc_pres_val	= c_func
obc_enth_type	= fixed_value
gamma		= 1.000001
dt_min		= 1e-20

# Problem-specific parameters
ring_mass	= 1.99e27	# mass of ring
init_temp	= 100.0		# temperature (not important to solution)
col_ratio	= 1e10          # ratio of column densities in and out of ring
ring_loc	= 7.5e13	# initial radius of ring
kinematic_visc	= 5.93e12       # chosen so dimensionless time t_s = 1e5 yr
end_time	= 0.128         # simulation run time in units of t_s